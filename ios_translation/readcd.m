function [r_mx, r_my, r_mz, r_mp, r_mt, r_parameters, r_timesteps, r_fileinfo] = readcd(name)
% returns [mx, my, mz, mp, mt, info_p, info_t, info_f]
% where:
% [mx,my,mz,mp,mt] are the dimensions of the data
% info_p contains the names of the parameters
% info_t contains the actual numbers of the timesteps
% info_f contains the info lines about the file history
% 
% name has to be without extension!

header

cfid = fopen([name,'.cd'],'r');
line = fgetl(cfid); %skip first line

% get dimensions
line = fgetl(cfid);
tmp = sscanf(line,'%*c m3 = %d m2 = %d m1 = %d'); 
r_mz = tmp(1);
r_my = tmp(2);
r_mx = tmp(3);
line = fgetl(cfid);
r_mp = sscanf(line,'%*s of parameters = %d');
line = fgetl(cfid);
r_mt = sscanf(line,'%*s of timesteps  = %d');

% get file info
line = fgetl(cfid);
while isempty(strfind(line,'Information about file :'))
    line = fgetl(cfid);
end
mf = sscanf(line,'%*s about file :   (%d %*s'); 
r_fileinfo = {};
for i=1:mf
    line = fgetl(cfid);
    r_fileinfo = [r_fileinfo  strtrim(line)];
end

% get parameter names
line = fgetl(cfid);
while isempty(strfind(line,'Information about parameters :'))
    line = fgetl(cfid);
end
r_parameters = {};
for i=1:r_mp
    line = fgetl(cfid);
    r_parameters = [r_parameters  strtrim(line)];
end

% get timestep numbers
line = fgetl(cfid);
r_timesteps = fscanf(cfid,'%d');
r_timesteps = r_timesteps';

fclose(cfid); %close .cd-file

end

