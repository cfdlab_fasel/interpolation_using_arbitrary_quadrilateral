function [x,y]=read_grid(filename)
[r_data, r_parameters, ~, ~, ~, ~, ~, mp, ~] = readios(filename);
disp('Parameters are: ');
disp('***********************');
for i=1:1:mp
    fprintf('%i, ',i);
    disp(r_parameters{i});
end
disp('***********************');

x = r_data(:,:,1,1);
y = r_data(:,:,1,2);
end