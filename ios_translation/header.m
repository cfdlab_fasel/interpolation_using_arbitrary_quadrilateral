% This header should be included by every script or function


% global Settings

%g_mform = 'b'; % dataformat 'b' for BigEndian \\ aha this was originally in the code
g_mform = 'n';

% aha: endianness ==> set in header
% 'n' or 'native'
% Your system byte ordering (default)
% 'b' or 'ieee-be'
% Big-endian ordering
% 'l' or 'ieee-le'
% Little-endian ordering
% 's' or 'ieee-be.l64'
% Big-endian ordering, 64-bit long data type
% 'a' or 'ieee-le.l64'
% Little-endian ordering, 64-bit long data type