function [name,ext,prec,bytes] = analysefilename(filename)
% Analyse and split filename of .s4 or .s8 files
% Returns [name,ext,prec,bytes] where
%
% name     string, filename without extension
% ext      string, fileextension (i.e 's4' or 's8')
% prec     string, precicsion according to ext ('single' or 'double')
% bytes    int, number of bytes per value (4 or 8)

header

l_str = length(filename);
if(l_str < 4 || filename(l_str-2) ~= '.')
    error('Bad filename or unknown file extension. (has to be .s4 or .s8)');
end

ext = filename(l_str-1:l_str);
name = filename(1:l_str-3);

if(ext == 's4')
    prec = 'single';
    bytes = 4;
elseif(ext == 's8')
    prec = 'double';
    bytes = 8;
else
    error('Unknown file extension. (has to be .s4 or .s8)');
end

end