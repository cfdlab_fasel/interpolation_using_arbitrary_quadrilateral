function lioscut(filenamein,filenameout,xcut,ycut,zcut,pcut,tcut,showprogress)
% Cuts a large ios file
% Parameters:
%
% filenamein, filenameout   strings for filenames
% xcut,ycut,zcut,pcut,tcut     index-vectors for cutting. Example:
%                              1:100 --> all the values from 1 to 100
%                              1:2:51 --> every second value from 1 to 51
%                              10 --> only slice number 10
%                              0 --> no cut in this dimension (or character :)
% showprogress      set to 1 for additional output. Otherwise 0 or nothing.
% By Michael Severin                            

header;
  
if (nargin < 8)
    showprogress = 0;
end

% ---------------
% Preparation
% ---------------

% analyse input file name
[name,~,prec,bytes] = analysefilename(filenamein);
% read input cd-file
[mx,my,mz,mp,mt,info_p,info_t,info_f] = readcd(name);

% Check wich dimensions to keep
if(xcut(1) == 0 || xcut(1) == ':')
    xcut = [1:mx];
    omx = mx;
else
    omx = length(xcut);
end

if(ycut(1) == 0 || ycut(1) == ':')
    ycut = [1:my];
    omy = my;
else
    omy = length(ycut);
end

if(zcut(1) == 0 || zcut(1) == ':')
    zcut = [1:mz];
    omz = mz;
else
    omz = length(zcut);
end

if(pcut(1) == 0 || pcut(1) == ':') 
    pcut = [1:mp];
    omp = mp;
else
    omp = length(pcut);
    info_p = info_p(pcut);
end

if(tcut(1) == 0 || tcut(1) == ':') 
    tcut = [1:mt];
    omt = mt;
else
    omt = length(tcut);
    info_t = info_t(tcut);
end

% analyse output file name
[name2,~,prec2,~] = analysefilename(filenameout); 

% create output cd-file    
infoline = ['MatLab: Cut ios to ', filenamein];
info_f = [info_f, infoline];
writecd(omx, omy, omz, omp, omt, name2, info_p, info_t, info_f)


% ------------
% Action
% ------------

% open input and output files
ifid = fopen(filenamein,'r');
ofid = fopen(filenameout,'w');

% allocate memory
odataslice = zeros(omx,omy,omz);

it_old = 0;
for oit=1:omt
    offset = mx*my*mz*mp*(tcut(oit)-it_old-1) * bytes;
    fseek(ifid,offset,'cof');
    it_old = tcut(oit);
    
    ip_old = 0;
    for oip=1:omp
        offset = mx*my*mz*(pcut(oip)-ip_old-1) * bytes;
        fseek(ifid,offset,'cof');
        ip_old = pcut(oip);
        
        iz_old = 0;
        for oiz=1:omz
            offset = mx*my*(zcut(oiz)-iz_old-1) * bytes;
            fseek(ifid,offset,'cof');
            iz_old = zcut(oiz);
            
            tmp = fread(ifid,[mx my],prec,0,g_mform);
            odataslice(:,:,oiz) = tmp(xcut,ycut);
        end     
        for oiz=1:omz
            fwrite(ofid,odataslice(:,:,oiz),prec2,0,g_mform);
        end
        if(iz_old < mz)
            offset = mx*my*(mz-iz_old) * bytes;
            fseek(ifid,offset,'cof');
        end
        
    end
    if(ip_old < mp)
        offset = mx*my*mz*(mp-ip_old) * bytes;
        fseek(ifid,offset,'cof');
    end
    if (showprogress == 1)
        disp(['it=',num2str(oit),' of ',num2str(omt),' done.']);
    end
end

fclose(ifid); %close files
fclose(ofid);

disp(['Done cutting! File saved to ',filenameout]);
end
