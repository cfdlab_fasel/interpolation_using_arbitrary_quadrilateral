function writeios(data, filename, info_p, info_t, info_f)
% writes data to an ios file
% with parameters:
% data      5D-array of size [mx,my,mz,mp,mt] containing the actual data
% filename  name of output file - has to be .s4 or .s8!    
% info_p    names of the parameters as string array
% info_t    actual numbers of the timesteps as integer array
% info_f    info lines about the file history as string array

header;

% ---------------------
% Preparation
% ---------------------

% check for number of arguments and set default values
if (nargin < 2)
    error('Not enough input arguments.');
end
if (nargin < 5)
    info_f = {};
end
if (nargin < 4)
    info_t = 1:size(data,5);
end
if (nargin < 3)
    info_p = {};
    for ip=1:size(data,4)
        tmp = ['param',num2str(ip)];
        info_p = [info_p tmp];
    end
end

% Analyse filename
[name,~,prec,~] = analysefilename(filename);


% Write .cd-file
[mx,my,mz,mp,mt] = size(data);
writecd(mx,my,mz,mp,mt,name,info_p,info_t,info_f);


% ----------------------------
% Write ios-file
% ----------------------------

ifid = fopen(filename,'w');

for it=1:mt
    for ip=1:mp
        for iz=1:mz
            fwrite(ifid,data(:,:,iz,ip,it),prec,0,g_mform);
        end
    end
end

fclose(ifid);

disp(['File ',filename,' successfully written!'])
end