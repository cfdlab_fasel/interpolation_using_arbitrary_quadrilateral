function writecd(mx, my, mz, mp, mt, name, info_p, info_t, info_f)
% writes cd-file
% with parameters:
% mx,my,my,mp,mt dimensions of data
% name      name of output file - no extension!    
% info_p    names of the parameters as string array
% info_t    actual numbers of the timesteps as integer array
% info_f    info lines about the file history as string array

header

% check for number of arguments and set default values
if (nargin < 6)
    error('Not enough input arguments.');
end
if (nargin < 9)
    info_f = {};
end
if (nargin < 8)
    info_t = 1:size(data,5);
end
if (nargin < 7)
    info_p = {};
    for ip=1:size(data,4)
        tmp = ['param',num2str(ip)];
        info_p = [info_p tmp];
    end
end

% ----------------------------------
% Write .cd-file
% ----------------------------------

cfid = fopen([name,'.cd'],'w');

% write dimensions
fprintf(cfid,'     size of array:\n');
fprintf(cfid,'     m3 = %5d     m2 = %5d     m1 = %5d\n',mz,my,mx);
fprintf(cfid,'     number of parameters = %5d\n',mp);
fprintf(cfid,'     number of timesteps  = %5d\n',mt);
fprintf(cfid,'\n');

% write file info
mf = length(info_f);
fprintf(cfid,'     Information about file :   ( %2d  info lines )\n',mf);
if(mf == 0)
    fprintf(cfid,'\n');
else
    for i_f=1:mf
        fprintf(cfid,'   %s\n',char(info_f(i_f)));
    end
end

% write parameter info
fprintf(cfid,'     Information about parameters :\n');
for ip=1:mp
    fprintf(cfid,'   %s\n',char(info_p(ip)));
end

% write timestep info
fprintf(cfid,'  Numbers of timesteps :\n');
fprintf(cfid,'  %10d  %10d  %10d  %10d  %10d  %10d\n',info_t);

fclose(cfid);   %close .cd-file

end
