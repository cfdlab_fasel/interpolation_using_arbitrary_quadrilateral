function [r_data, r_parameters, r_timesteps, r_fileinfo, mx, my, mz, mp, mt] = readios(filename)
% returns [data, info_p, info_t, info_f] 
% where:
% data is a 5D-array of size [mx,my,mz,mp,mt] containing the actual data
% info_p contains the names of the parameters
% info_t contains the actual numbers of the timesteps
% info_f contains the info lines about the file history
% 
% filename has to be *.s4 or *.s8!

header;

[name,~,prec,~] = analysefilename(filename);
[mx,my,mz,mp,mt,r_parameters,r_timesteps,r_fileinfo] = readcd(name);

% ----------------------------
% Read ios-file
% ----------------------------

ifid = fopen(filename,'r');

%r_data = zeros(mx,my,mz,mp,mt); %allocate memory
%for it=1:mt
%    for ip=1:mp
%        for iz=1:mz
%            r_data(:,:,iz,ip,it) = fread(ifid,[mx,my],prec,0,g_mform);
%        end
%    end
%end

r_data = reshape(fread(ifid,mx*my*mz*mp*mt,prec,0,g_mform),[mx my mz mp mt]);

fclose(ifid);

disp(['File ',filename,' read.']);
end