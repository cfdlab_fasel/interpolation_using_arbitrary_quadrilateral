function [wall_slope]=read_wall_slope(filename)
[r_data, r_parameters, ~, ~, ~, ~, ~, mp, ~] = readios(filename);
disp('Parameters are: ');
disp('***********************');
for i=1:1:mp
    fprintf('%i, ',i);
    disp(r_parameters{i});
end
disp('***********************');

wall_slope = r_data';