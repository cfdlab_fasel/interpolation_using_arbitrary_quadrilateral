function [u,v,p,t]=read_flowfield(filename)
[r_data, r_parameters, ~, ~, mx, my, mz, mp, mt] = readios(filename);
disp('Parameters are: ');
disp('***********************');
for i=1:1:mp
    fprintf('%i, ',i);
    disp(r_parameters{i});
end
disp('***********************');

i_u=input('The index of u velocity is: ');
i_v=input('The index of v velocity is: ');
i_p=input('The index of pressure is: ');
i_t=input('The index of temperature is: ');
%
% r_data=reshape(r_data,[1,mx*my*mz*mp*mt]);
% r_data=reshape(r_data,[mx,my,mz,mp,mt]);
for j=1:1:my
%     j
%     if (j==134)
%         fprintf('132\n')
%     end
    u(:,j) = r_data(:,rem(i_u+mp*(j-1),my)+(rem(i_u+mp*(j-1),my)==0)*my,...
        1,ceil((i_u+mp*(j-1))/my));
    v(:,j) = r_data(:,rem(i_v+mp*(j-1),my)+(rem(i_v+mp*(j-1),my)==0)*my,...
        1,ceil((i_v+mp*(j-1))/my));
    p(:,j) = r_data(:,rem(i_p+mp*(j-1),my)+(rem(i_p+mp*(j-1),my)==0)*my,...
        1,ceil((i_p+mp*(j-1))/my));
    t(:,j) = r_data(:,rem(i_t+mp*(j-1),my)+(rem(i_t+mp*(j-1),my)==0)*my,...
        1,ceil((i_t+mp*(j-1))/my));
%     u(:,j) = r_data(:,j,1,i_u);
%     v(:,j) = r_data(:,j,1,i_v);
%     p(:,j) = r_data(:,j,1,i_p);
%     t(:,j) = r_data(:,j,1,i_t);
end
