clear;clc
%% required inputs
grid_file_fv  = 'grid_fv_swapped.s8';
grid_file_dns = 'grid_gc_swapped.s8';
dat_file_fv   = 'prim_nt_10e6.s8';
output_file_name = 'interpolated.s8';

%% read data files
current_folder = cd;
cd ../ios_translation

% read fv grid
grid_fv=readios(grid_file_fv);
x_fv = grid_fv(:,:,1,1);
y_fv = grid_fv(:,:,1,2);

% read dns grid
grid_dns=readios(grid_file_dns);
x_dns = grid_dns(:,:,1,1);
y_dns = grid_dns(:,:,1,2);

% read flow field
dat_fv = readios(dat_file_fv);
par = dat_fv(:,:,1,:);

%% Interpolation
cd(current_folder);
% Searching for indices of grid vertices around target grid point
[i_left, j_left] = locate_indices(x_fv, y_fv, x_dns, y_dns);

% Interpolate in arbitrary quadrilateral
par_i = interpolation_arbitrary_quadrilateral(par, x_fv, y_fv, i_left, j_left, x_dns, y_dns);

% Write interpolated data
write_any(par_i, output_file_name);
