function write_any(par_i,output_file_name)
[mx,my,mp] = size(par_i);
mz = 1;
mt = 1;
data_tmp=zeros(mx,my,mz,mp,mt);

for j = 1:my
    for i = 1:mx
        for k=1:6
            data_tmp(i,j,1,k,1)= par_i(i,j,k);
        end
    end
end
current_folder = cd;
cd ../ios_translation/
writeios(data_tmp, output_file_name, ['uvel';'vvel';'pres';'dens';'temp';'mach'], 1, '')
fprintf('The file %s is generated in the folder %s \n',output_file_name, cd);
cd(current_folder);
end
