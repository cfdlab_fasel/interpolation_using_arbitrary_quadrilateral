function [i_left, j_below] = locate_indices(x_fv, y_fv, x_dns, y_dns)
% searching for indices of grid vertices around target grid point
% ------OUTPUTS------
% i_left:   index of left grid vertice in x direction
% j_below:  index of grid vertice below in y direction
% ------INPUTS------
% x_fv:     x-coordinates of input grid
% y_fv:     y-coordinates of input grid
% x_dns:    x-coordinates of output grid
% y_dns:    y-coordinates of output grid

% Get the size of input grid and output grids
[nx_fv, ny_fv]   = size(x_fv);
[nx_dns, ny_dns] = size(x_dns);

%% locating the surrounding indices
% Computing wall normal distance
h_dns= sqrt( (x_dns(1,:)-x_dns(1,1)).^2 + (y_dns(1,:)-y_dns(1,1)).^2);
h_fv = sqrt( (x_fv(1,:)-x_fv(1,1)).^2 + (y_fv(1,:)-y_fv(1,1)).^2);

% x-coordinates on the wall
x_wall_fv  = x_fv(:, 1);
x_wall_dns = x_dns(:, 1);

% locating left indices
i_left = zeros(1,nx_dns);
current_k = 1; 

for i=1 : nx_dns
    for k = current_k : nx_fv-1
        if (x_wall_dns(i) >= x_wall_fv(k)) && (x_wall_dns(i) <= x_wall_fv(k+1))
            i_left(i)  = k;
            break;
        end
    end
    current_k = k;
% checking byteswap
    if (i > 1)
        if (i_left(i) - i_left(i-1) > 1) 
            fprintf('Index increment is %i at i=%i, check whether your grid has been byteswapped. \n',i_left(i) - i_left(i-1),i);
        end
    end
end

% locating indices below
j_below = zeros(1,ny_dns);
current_k = 1; 

for j=1:ny_dns
    for k = current_k : ny_fv-1
        if (h_dns(j) >= h_fv(k)) && (h_dns(j) <= h_fv(k+1))
            j_below(j)  = k;
            break
        end
    end
    current_k = k; 
% checking byteswap
    if (j > 1)
        if (j_below(j) - j_below(j-1) > 1) 
            fprintf('Index increment is %i at j=%i, check whether your grid has been byteswapped. \n',j_below(j) - j_below(j-1),j);
        end
    end
end

fprintf('Surrounding indices have been located. \n');
end
