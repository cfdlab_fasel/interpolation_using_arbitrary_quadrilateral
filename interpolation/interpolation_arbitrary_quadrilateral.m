function par_i = interpolation_arbitrary_quadrilateral(par, x_from, y_from, i_left, j_below, x_to, y_to)
% Interpolate in arbitrary quadrilateral
%     par:        data in flow field
%     x_from:     the x-coordinates in flow field
%     y_from:     the y-coordinates in flow field
%     i_left:     the left index in x direction
%     j_below:     the left index in y direction
%     x_to:       the x-coordinates in targeted computational domain
%     y_to:       the y-coordinates in targeted computational domain

% number of points in targeted computational domain
[nx_to, ny_to] = size(x_to);

% number of parameters in flow field
nv = size(par, 4);

fprintf('Starting interpolation. \n');
par_i = zeros(nx_to, ny_to);
for i = 1 : nx_to
    for j = 1 : ny_to
%       fprintf('%i, %i, %i, %i\n',i,j,i_left(i),j_left(j)); % debug
        for k = 1 : nv
            par_temp = par(:, :, 1, k);
            
            % coordinates of quatrilateral nodes
            px = [x_from(i_left(i), j_below(j)  ),...
                x_from(i_left(i)+1, j_below(j)  ),...
                x_from(i_left(i)+1, j_below(j)+1),...
                x_from(i_left(i)  , j_below(j)+1)];
            
            py = [y_from(i_left(i), j_below(j)  ),...
                y_from(i_left(i)+1, j_below(j)  ),...
                y_from(i_left(i)+1, j_below(j)+1),...
                y_from(i_left(i)  , j_below(j)+1)];
            
            % computing parameters for mapping
            A = [1 0 0 0; 1 1 0 0; 1 1 1 1; 1 0 1 0];
            a = A\px';
            b = A\py';
            
            % values on quadrilateral nodes
            v = [par_temp(i_left(i) , j_below(j)  ),...
                par_temp(i_left(i)+1, j_below(j)  ),...
                par_temp(i_left(i)+1, j_below(j)+1),...
                par_temp(i_left(i)  , j_below(j)+1)];
            
            % forcing u/v velocity to be zeros on the wall
            if (j==1) && (k==2 || k==1)
                if (i==1)
                    fprintf('Force u/v velocity (No. %i parameter) on the wall to be zeros.\n',k);
                end
                [par_i(i, j, k)] = 0;
            else
                % mapping from the physical coordinate to logical coordinate
                [l, m] = mapping(x_to(i, j), y_to(i, j), a, b);
                % gathering data
                par_i(i, j, k) = v(1) * (1-l)*(1-m) + v(2) *l*(1-m) + ...
                    v(3) *l*m + v(4)*(1-l)*m;
            end
        end
    end
    if (i==1 || rem(i, 50)==0 || i==nx_to)
        fprintf('Interpolation processed %4i/%i ...\n', i, nx_to); 
    end
end
end