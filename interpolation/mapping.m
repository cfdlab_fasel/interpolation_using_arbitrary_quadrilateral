function [l, m] = mapping(x,y,a,b)
% mapping from the physical coordinate to logical coordinate
% ------OUTPUTS-------
% [l,m]:            is the logical coordinate of the target point
% ------INPUTs------
% (x,y):            the physical coordinate of target point
% (a(1:4),b(1:4)):  is the physical coordinates of the quadrilateral
% v:                the values at the corners of the quadrilateral

% quadratic equation coeffs, aa*mm^2+bb*m+cc=0
aa = a(4)*b(3) - a(3)*b(4);
bb = a(4)*b(1) - a(1)*b(4) + a(2)*b(3) - a(3)*b(2) + x*b(4) - y*a(4);
cc = a(2)*b(1) - a(1)*b(2) + x*b(2) - y*a(2);

% computing m = (-b+sqrt(b^2-4ac))/(2a)
%   For parallelogram, a(4) = b(4) and a(3) = b(3), so aa = 0. 
%   Then m = -cc/bb.
if (aa == 0)
    m = - cc/bb;
else
    det = sqrt(bb*bb - 4*aa*cc);
    m = (-bb+det)/(2*aa);
end

%compute l
l = (x-a(1)-a(3)*m)/(a(2)+a(4)*m);
end